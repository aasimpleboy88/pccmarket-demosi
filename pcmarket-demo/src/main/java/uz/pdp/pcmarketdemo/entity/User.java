package uz.pdp.pcmarketdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.pcmarketdemo.enums.Role;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Enumerated(value = EnumType.STRING)
    private Role roleName;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false,unique = true)
    private String login;

    @Column(nullable = false,unique = true)
    private String email;
}
