package uz.pdp.pcmarketdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcmarketDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcmarketDemoApplication.class, args);
    }

}
