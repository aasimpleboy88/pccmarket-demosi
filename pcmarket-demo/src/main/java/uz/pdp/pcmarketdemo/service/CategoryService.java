package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.pcmarketdemo.entity.Category;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.CategoryDto;
import uz.pdp.pcmarketdemo.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public ApiResponse getAllCategory(){
        List<Category> categoryList = categoryRepository.findAll();
        return new ApiResponse("Barcha categorylar ro`yxati",true,categoryList);
    }

    public ApiResponse getCategoryById(Integer id){
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()){
            Category category = optionalCategory.get();
            return new ApiResponse(id+"-ID dagi Category",true,category);
        }else {
            return new ApiResponse(id+"-ID dagi category mavjud emas",false);
        }
    }

    public ApiResponse getPageableCategory(int page, int size){
        Pageable pageable= PageRequest.of(page, size);
        Page<Category> categoryPage = categoryRepository.findAll(pageable);
        return new ApiResponse(page+"-pagedagi Categorylar ro`yxati",true,categoryPage);
    }

    public ApiResponse deleteCategory(Integer id){
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()){
            categoryRepository.deleteById(id);
            return new ApiResponse(id+"-ID dagi Category o`chirildi",true);
        }else {
            return new ApiResponse(id+"-ID dagi Category mavjud emas",false);
        }
    }

    public ApiResponse saveNewCategory(CategoryDto categoryDto){
        Optional<Category> optionalCategory = categoryRepository.findById(categoryDto.getParentCategoryId());
        if (optionalCategory.isPresent()){
            Category category=new Category();
            Category parentCategory = optionalCategory.get();
            category.setParentCategory(parentCategory);
            category.setActive(categoryDto.isActive());
            category.setName(categoryDto.getName());
            categoryRepository.save(category);
            return new ApiResponse("Category saqlandi",true);
        }else {
            return new ApiResponse(categoryDto.getParentCategoryId()+"- ParentCategoryId xato kiritildi ",false);
        }
    }


    public ApiResponse editCategory(Integer id, CategoryDto categoryDto){
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()){
            Optional<Category> optionalParentCategory= categoryRepository.findById(categoryDto.getParentCategoryId());
            if (optionalParentCategory.isPresent()){
                Category category = optionalCategory.get();
                category.setName(categoryDto.getName());
                category.setActive(categoryDto.isActive());
                Category parentCategory = optionalParentCategory.get();
                category.setParentCategory(parentCategory);
                categoryRepository.save(category);
                return new ApiResponse(id+"-ID dagi Category ma`lumotlari o`zgartirildi",true);
            }else {
                return new ApiResponse("ParentCategoryId xato kiritildi",false);
            }
        }else {
            return new ApiResponse(id+"-ID dagi Category mavjud emas",false);
        }
    }



}
