package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.pcmarketdemo.entity.Attachment;
import uz.pdp.pcmarketdemo.entity.AttachmentContent;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.repository.AttachmentContentRepository;

import java.io.IOException;

@Service
public class AttachmentContentService {

    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    public ApiResponse uploadAttachmentContent(MultipartFile file, Attachment savedAttachment) throws IOException {
        AttachmentContent attachmentContent=new AttachmentContent();
        attachmentContent.setAttachment(savedAttachment);
        attachmentContent.setBytes(file.getBytes());
        attachmentContentRepository.save(attachmentContent);
        return new ApiResponse("AttachmentContent saqlandi",true);
    }
}
