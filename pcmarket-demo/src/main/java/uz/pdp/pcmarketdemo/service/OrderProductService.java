package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.pcmarketdemo.entity.Order;
import uz.pdp.pcmarketdemo.entity.OrderProducts;
import uz.pdp.pcmarketdemo.entity.Product;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.OrderProductDto;
import uz.pdp.pcmarketdemo.repository.OrderProductRepository;
import uz.pdp.pcmarketdemo.repository.OrderRepository;
import uz.pdp.pcmarketdemo.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OrderProductService {

    @Autowired
    OrderProductRepository orderProductRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderRepository orderRepository;


    public ApiResponse getAllOrderProducts(){
        List<OrderProducts> orderProductsList = orderProductRepository.findAll();
        return new ApiResponse("Barcha orderProductlar ro`yxati",true,orderProductsList);
    }

    public ApiResponse getOrderProductById(Integer id){
        Optional<OrderProducts> orderProductRepositoryById = orderProductRepository.findById(id);
        if (orderProductRepositoryById.isPresent()){
            OrderProducts orderProducts = orderProductRepositoryById.get();
            return new ApiResponse(id+"-ID dagi orderProducts",true,orderProducts);
        }else {
            return new ApiResponse(id+"-ID dagi orderProducts mavjud emas",false);
        }
    }

    public ApiResponse getPageableOrderProducts(int page, int size){
        Pageable pageable= PageRequest.of(page, size);
        Page<OrderProducts> orderProductsPage = orderProductRepository.findAll(pageable);
        return new ApiResponse(page+"-pagedagi orderProductlar",true,orderProductsPage);
    }

    public ApiResponse deleteOrderProducts(Integer id){
        Optional<OrderProducts> orderProductRepositoryById = orderProductRepository.findById(id);
        if (orderProductRepositoryById.isPresent()){
            orderProductRepository.deleteById(id);
            return new ApiResponse(id+"-ID dagi orderProduct o`chirildi",true);
        }else {
            return new ApiResponse(id+"-ID dagi orderProduct mavjud emas",false);
        }
    }

    public ApiResponse saveOrderProduct(OrderProductDto orderProductDto){
        Optional<Product> optionalProduct = productRepository.findById(orderProductDto.getProductId());
        if (optionalProduct.isPresent()){
            Optional<Order> optionalOrder = orderRepository.findById(orderProductDto.getOrderId());
            if (optionalOrder.isPresent()){
                OrderProducts orderProducts=new OrderProducts();
                orderProducts.setAmount(orderProductDto.getAmount());
                Order order = optionalOrder.get();
                orderProducts.setOrder(order);
                Product product = optionalProduct.get();
                orderProducts.setProduct(product);
                orderProductRepository.save(orderProducts);
                return new ApiResponse("Yangi orderProducts saqlandi",true);
            }else {
                return new ApiResponse(orderProductDto.getOrderId()+"-ID dagi order mavjud emas",false);
            }
        }else {
            return new ApiResponse(orderProductDto.getProductId()+"-ID dagi product mavjud emas",false);
        }
    }

    public ApiResponse editOrderProducts(Integer id,OrderProductDto orderProductDto){
        Optional<OrderProducts> optionalOrderProducts = orderProductRepository.findById(id);
        if (optionalOrderProducts.isPresent()){
            Optional<Product> optionalProduct = productRepository.findById(orderProductDto.getProductId());
            if (optionalProduct.isPresent()){
                Optional<Order> optionalOrder = orderRepository.findById(orderProductDto.getOrderId());
                if (optionalOrder.isPresent()){
                    OrderProducts orderProducts = optionalOrderProducts.get();
                    orderProducts.setAmount(orderProductDto.getAmount());
                    Order order = optionalOrder.get();
                    orderProducts.setOrder(order);
                    Product product = optionalProduct.get();
                    orderProducts.setProduct(product);
                    orderProductRepository.save(orderProducts);
                    return new ApiResponse(id+"-ID dagi orderProducts ma`lumotlari o`zgartirildi",true);
                }else {
                    return new ApiResponse(orderProductDto.getOrderId()+"-ID dagi order mavjud emas",false);
                }
            }else {
                return new ApiResponse(orderProductDto.getProductId()+"-ID dagi product mavjud emas",false);
            }
        }else {
            return new ApiResponse(id+"-ID dagi orderProduct mavjud emas",false);
        }
    }
}
