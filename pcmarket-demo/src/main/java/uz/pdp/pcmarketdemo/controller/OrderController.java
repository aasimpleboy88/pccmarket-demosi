package uz.pdp.pcmarketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.OrderDto;
import uz.pdp.pcmarketdemo.service.OrderService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("/getAllOrders")
    public HttpEntity<ApiResponse> getAllOrders(){
        ApiResponse apiResponse = orderService.getAllOrders();
        return ResponseEntity.status(200).body(apiResponse);
    }

    @GetMapping("/getOrderById/{id}")
    public HttpEntity<ApiResponse> getOrderByID(@PathVariable Integer id){
        ApiResponse orderById = orderService.getOrderById(id);
        if (orderById.isSuccess()){
            return ResponseEntity.status(200).body(orderById);
        }else {
            return ResponseEntity.status(409).body(orderById);
        }
    }

    @GetMapping("/getOrderPageable")
    public HttpEntity<ApiResponse> getOrderPageable(@RequestParam int page,int size){
        ApiResponse pageableOrder = orderService.getPageableOrder(page, size);
        if (pageableOrder.isSuccess()){
            return ResponseEntity.status(200).body(pageableOrder);
        }else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(pageableOrder);
        }
    }

    @DeleteMapping("/deleteOrder/{id}")
    public HttpEntity<ApiResponse> deleteOrder(@PathVariable Integer id){
        ApiResponse apiResponse = orderService.deleteOrder(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(200).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PostMapping("/saveNewOrder")
    public HttpEntity<ApiResponse> saveNewOrder(@Valid @RequestBody OrderDto orderDto){
        ApiResponse apiResponse = orderService.saveNewOrder(orderDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PutMapping("/editOrder/{id}")
    public HttpEntity<ApiResponse> editOrder(@PathVariable Integer id, @Valid @RequestBody OrderDto orderDto){
        ApiResponse apiResponse = orderService.editOrder(id, orderDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
