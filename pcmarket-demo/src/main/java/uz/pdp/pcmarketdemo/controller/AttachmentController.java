package uz.pdp.pcmarketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @GetMapping("/getAllAttachment")
    public HttpEntity<ApiResponse> getAllAttachment(){
        ApiResponse allAttachment = attachmentService.getAllAttachment();
        return ResponseEntity.status(200).body(allAttachment);
    }

    @GetMapping("/getAttachmentById/{id}")
    public HttpEntity<ApiResponse> getAttachmentById(@PathVariable Integer id){
        ApiResponse attachmentById = attachmentService.getAttachmentById(id);
        if (attachmentById.isSuccess()){
            return ResponseEntity.status(200).body(attachmentById);
        }else {
            return ResponseEntity.status(404).body(attachmentById);
        }
    }

    @GetMapping("/getPageableAttachment")
    public HttpEntity<ApiResponse> getPageAbleAttechment(@RequestParam int page, int size){
        ApiResponse pageableAttachment = attachmentService.getPageableAttachment(page, size);
        return ResponseEntity.status(200).body(pageableAttachment);
    }

    @DeleteMapping("/deleteAttachment/{id}")
    public HttpEntity<ApiResponse> deleteAttachment(@PathVariable Integer id){
        ApiResponse apiResponse = attachmentService.deleteAttachment(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(204).body(apiResponse);
        }else {
            return ResponseEntity.status(404).body(apiResponse);
        }
    }

    @PostMapping("/uploadAttachment")
    public HttpEntity<ApiResponse> uploadAttachment(MultipartHttpServletRequest request) throws IOException {
        ApiResponse apiResponse = attachmentService.uploadFile(request);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @GetMapping("/downloadAttachment/{id}")
    public void downloadAttachment(@PathVariable Integer id, HttpServletResponse response) throws IOException {
        attachmentService.downloadAttachment(id,response);
    }
}
