package uz.pdp.pcmarketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.CategoryDto;
import uz.pdp.pcmarketdemo.service.CategoryService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("/getAllCategory")
    public HttpEntity<ApiResponse> getAllCategory(){
        ApiResponse allCategory = categoryService.getAllCategory();
        return ResponseEntity.status(200).body(allCategory);
    }

    @GetMapping("/getCategoryById/{id}")
    public HttpEntity<ApiResponse> getCategoryById(@PathVariable Integer id){
        ApiResponse categoryById = categoryService.getCategoryById(id);
        if (categoryById.isSuccess()){
            return ResponseEntity.status(200).body(categoryById);
        }else {
            return ResponseEntity.status(409).body(categoryById);
        }
    }

    @GetMapping("/getPageableCategory")
    public HttpEntity<ApiResponse> getPageableCategory(@RequestParam int page, int size){
        ApiResponse pageableCategory = categoryService.getPageableCategory(page, size);
        return ResponseEntity.status(200).body(pageableCategory);
    }

    @DeleteMapping("/deleteCategory/{id}")
    public HttpEntity<ApiResponse> deleteCategory(@PathVariable Integer id){
        ApiResponse apiResponse = categoryService.deleteCategory(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(204).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }


    @PostMapping("/saveNewCategory")
    public HttpEntity<ApiResponse> saveNewCategory(@Valid @RequestBody CategoryDto categoryDto){
        ApiResponse apiResponse = categoryService.saveNewCategory(categoryDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PutMapping("/editCategory/{id}")
    public HttpEntity<ApiResponse> editCategory(@PathVariable Integer id, @Valid @RequestBody CategoryDto categoryDto){
        ApiResponse apiResponse = categoryService.editCategory(id, categoryDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
