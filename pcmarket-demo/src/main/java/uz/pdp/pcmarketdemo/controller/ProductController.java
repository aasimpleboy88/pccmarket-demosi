package uz.pdp.pcmarketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.ProductDto;
import uz.pdp.pcmarketdemo.service.ProductService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductService productService;


    @GetMapping("/getAllProduct")
    public HttpEntity<ApiResponse> getAllProduct(){
        ApiResponse allProduct = productService.getAllProduct();
        return ResponseEntity.status(200).body(allProduct);
    }

    @GetMapping("/getProductById/{id}")
    public HttpEntity<ApiResponse> getProductById(@PathVariable Integer id){
        ApiResponse productById = productService.getProductById(id);
        if (productById.isSuccess()){
            return ResponseEntity.status(200).body(productById);
        }else {
            return ResponseEntity.status(409).body(productById);
        }
    }

    @GetMapping("/getPageableProduct")
    public HttpEntity<ApiResponse> getPageableProduct(@RequestParam int page, int size){
        ApiResponse pageableProduct = productService.getPageableProduct(page, size);
        return ResponseEntity.status(200).body(pageableProduct);
    }

    @DeleteMapping("/deleteProduct/{id}")
    public HttpEntity<ApiResponse> deleteProduct(@PathVariable Integer id){
        ApiResponse apiResponse = productService.deleteProduct(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(204).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PostMapping("/saveNewProduct")
    public HttpEntity<ApiResponse> saveNewProduct(@Valid @RequestBody ProductDto productDto){
        ApiResponse apiResponse = productService.saveNewProduct(productDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PutMapping("/editProduct/{id}")
    public HttpEntity<ApiResponse> editProduct(@PathVariable Integer id,@Valid @RequestBody ProductDto productDto){
        ApiResponse apiResponse = productService.editProduct(id, productDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
