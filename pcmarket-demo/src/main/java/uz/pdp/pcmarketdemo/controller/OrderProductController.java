package uz.pdp.pcmarketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.OrderProductDto;
import uz.pdp.pcmarketdemo.service.OrderProductService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/orderProduct")
public class OrderProductController {

    @Autowired
    OrderProductService orderProductService;

    @GetMapping("/getAllOrderProducts")
    public HttpEntity<ApiResponse> getAllOrderProducts(){
        ApiResponse allOrderProducts = orderProductService.getAllOrderProducts();
        return ResponseEntity.status(200).body(allOrderProducts);
    }

    @GetMapping("/getOrderProductsById/{id}")
    public HttpEntity<ApiResponse> getOrderProductsById(@PathVariable Integer id){
        ApiResponse orderProductById = orderProductService.getOrderProductById(id);
        if (orderProductById.isSuccess()){
            return ResponseEntity.status(200).body(orderProductById);
        }else {
            return ResponseEntity.status(409).body(orderProductById);
        }
    }


    @GetMapping("/getPageableOrderProducts")
    public HttpEntity<ApiResponse> getPageableOrderProducts(@RequestParam int page, int size){
        ApiResponse pageableOrderProducts = orderProductService.getPageableOrderProducts(page, size);
        return ResponseEntity.status(200).body(pageableOrderProducts);
    }

    @DeleteMapping("/deleteOrderProducts/{id}")
    public HttpEntity<ApiResponse> deleteOrderProducts(@PathVariable Integer id){
        ApiResponse apiResponse = orderProductService.deleteOrderProducts(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(204).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PostMapping("/saveNewOrderProducts")
    public HttpEntity<ApiResponse> saveNewOrderProducts(@Valid @RequestBody OrderProductDto orderProductDto){
        ApiResponse apiResponse = orderProductService.saveOrderProduct(orderProductDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PutMapping("/editOrderProducts/{id}")
    public HttpEntity<ApiResponse> editOrderProducts(@PathVariable Integer id,@Valid @RequestBody OrderProductDto orderProductDto){
        ApiResponse apiResponse = orderProductService.editOrderProducts(id, orderProductDto);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


}
