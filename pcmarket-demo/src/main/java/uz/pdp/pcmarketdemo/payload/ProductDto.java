package uz.pdp.pcmarketdemo.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProductDto {
    @NotNull(message = "name bo`sh bo`lmasligi kerak")
    private String name;
    @NotNull(message = "description bo`sh bo`lmasligi kerak")
    private String description;
    @NotNull(message = "info bo`sh bo`lmasligi kerak")
    private String info;
    @NotNull(message = "attachmentId bo`sh bo`lmasligi kerak")
    private Integer attachmentId;
    @NotNull(message = "price bo`sh bo`lmasligi kerak")
    private double price;
    @NotNull(message = "categoryId bo`sh bo`lmasligi kerak")
    private Integer categoryId;

}
