package uz.pdp.pcmarketdemo.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryDto {

    @NotNull(message = "name bo`sh bo`lmasligi kerak")
    private String name;
    @NotNull(message = "parentCategory bo`sh bo`lmasligi kerak")
    private Integer parentCategoryId;
    @NotNull(message = "active bo`sh bo`lmasligi kerak")
    private boolean active;

}
