package uz.pdp.pcmarketdemo.payload;

import lombok.Data;
import uz.pdp.pcmarketdemo.enums.Role;

import javax.validation.constraints.NotNull;

@Data
public class UserDto {

    @NotNull(message = "name bo`sh bo`lmasligi kerak")
    private String name;
    @NotNull(message = "roleName bo`sh bo`lmasligi kerak")
    private Role roleName;
    @NotNull(message = "password bo`sh bo`lmasligi kerak")
    private String password;
    @NotNull(message = "login bo`sh bo`lmasligi kerak")
    private String login;
    @NotNull(message = "email bo`sh bo`lmasligi kerak")
    private String email;

}
