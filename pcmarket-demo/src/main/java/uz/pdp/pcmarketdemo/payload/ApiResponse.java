package uz.pdp.pcmarketdemo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApiResponse {
    private String massage;
    private boolean success;
    private Object object;


    public ApiResponse(String massage, boolean success) {
        this.massage = massage;
        this.success = success;
    }
}
