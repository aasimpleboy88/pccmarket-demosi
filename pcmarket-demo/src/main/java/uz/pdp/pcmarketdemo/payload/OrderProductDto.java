package uz.pdp.pcmarketdemo.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OrderProductDto {

    @NotNull(message = "productId bo`sh bo`lmasligi kerak")
    private Integer productId;
    @NotNull(message = "amount bo`sh bo`lmasligi kerak")
    private int amount;
    @NotNull(message = "orderId bo`sh bo`lmasligi kerak")
    private Integer orderId;

}
