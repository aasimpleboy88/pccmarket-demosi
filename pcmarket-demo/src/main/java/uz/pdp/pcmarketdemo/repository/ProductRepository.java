package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.Product;

public interface ProductRepository extends JpaRepository<Product,Integer> {
}
