package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.User;

public interface UserRepository extends JpaRepository<User,Integer> {

    boolean existsByEmail(String email);
    boolean existsByLogin(String login);

    boolean existsByLoginAndIdNot(String login, Integer id);
    boolean existsByEmailAndIdNot(String email, Integer id);

}
