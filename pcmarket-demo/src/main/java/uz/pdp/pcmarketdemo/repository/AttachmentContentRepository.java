package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.AttachmentContent;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Integer> {

    AttachmentContent findByAttachmentId(Integer attachment_id);
}
