package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
