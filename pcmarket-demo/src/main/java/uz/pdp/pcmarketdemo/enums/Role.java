package uz.pdp.pcmarketdemo.enums;

public enum Role {
    SUPERADMIN,
    ADMIN,
    USER
}
